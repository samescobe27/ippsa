<?php

// meta tags
$titulo = "Productos";
$descrip = "";
$imagen = "";


include('top.php');
include('header.php');

?>

<section class="banner-productos" style="background-image: url(assets/images/banner-productos.jpg);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-12 col-md-8 col-lg-6  block wow fadeInRight">
				<h1>Conoce nuestros <br>productos</h1>
				<span>Celdas de carga, Básculas, Indicadores, Accesorios y Diseños especiales</span>
			</div>
		</div>
	</div>
</section> 


<section class="productos">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<h2>Productos</h2>
			</div>
			<div class="col-md-10 search-products">
				<input class="button-search  d-md-block d-lg-block" name="buscar" id="buscar" type="text" placeholder="Buscar producto" class="form-control">
				<a href="#" class="search-icon  d-md-block d-lg-block">
					<img src="assets/images/lupa.svg"  widht="28" height="20" >
				</a>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-2">
				<div class="menu-productos">
			
					<a class="btn btn-titulo" data-toggle="collapse" href="#marcas" role="button" aria-expanded="false" aria-controls="marcas">
						Marcas <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="marcas">
						<div class="button-group filter-button-group list-produ">
							<button class="button is-checked" data-filter=".marcas">Marcas</button>
							<!-- <button class="button" data-filter=".todos">Todos los productos</button> -->
							<button class="button" data-filter=".ippsa">Ippsa</button>
							<button class="button " data-filter=".avery">Avery Weight Tronix</button>
							<button class="button" data-filter=".vpg">VPG</button>
							<!-- <button class="button" data-filter=".btek">B Tek</button> -->
							<button class="button " data-filter=".rinstrum">Rinstrum</button>
							<button class="button" data-filter=".pavone">Pavone</button>
							<!-- <button class="button" data-filter=".salter">Salter Brecknell</button> -->
						</div>
					</div>
					
					<!-- E M P I E Z A    B A S C U L A S -->
					<a class="btn btn-titulo" data-toggle="collapse" href="#basculas" role="button" aria-expanded="false" aria-controls="basculas">
						Básculas <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="basculas">
						<div class="button-group filter-button-group list-produ">
							<!-- <button class="button" data-filter=".basculas">Todos</button> -->
							<button class="button" data-filter=".camionera">Camioneras</button>
							<button class="button" data-filter=".ferro">Ferrocarrileras</button>
							<button class="button" data-filter=".piso">Piso</button>
							<button class="button" data-filter=".mesa">Mesa</button>
							<button class="button" data-filter=".ganadera">Ganadera</button>
							<button class="button" data-filter=".peso">Checadoras de Peso</button>
						</div>
					</div>

					<!-- E M P I E Z A    C E L D A S -->
					<a class="btn btn-titulo" data-toggle="collapse" href="#celdas" role="button" aria-expanded="false" aria-controls="celdas">
						Celdas <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="celdas">
						<div class="button-group filter-button-group list-produ">
							<!-- <button class="button" data-filter=".celdas">Todos</button> -->
							<button class="button" data-filter=".compresion">Compresión</button>
							<button class="button" data-filter=".single">Single Point</button>
							<button class="button" data-filter=".damped">Damped</button>
							<button class="button" data-filter=".share">Share Beams</button>
							<button class="button" data-filter=".tipo-s">Tipo S</button>
							<button class="button" data-filter=".vishay">Celdas Vishay</button>
							<button class="button" data-filter=".zemic">Celdas Zemic</button>
						</div>
					</div>

					

					<!-- E M P I E Z A    I N D I C A D O R E S -->
					<a class="btn btn-titulo" data-toggle="collapse" href="#indicadores" role="button" aria-expanded="false" aria-controls="indicadores">
						Indicadores <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="indicadores">
						<div class="button-group filter-button-group list-produ">
							<!-- <button class="button" data-filter=".indicadores">Todos</button> -->
							<!-- <button class="button" data-filter=".serie">Serie Go</button> -->
							<button class="button" data-filter=".zm">Avery Weigh-Tronix</button>
							<button class="button" data-filter=".rinstrum">Rinstum</button>
						</div>
					</div>

					<!-- E M P I E Z A    A C C E S O R I OS  -->
					<a class="btn btn-titulo" data-toggle="collapse" href="#accesorios" role="button" aria-expanded="false" aria-controls="accesorios">
						Accesorios <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="accesorios">
						<div class="button-group filter-button-group list-produ">
							<!-- <button class="button" data-filter=".accesorios">Todos</button> -->
							<button class="button" data-filter=".cables">Cables</button>
							<button class="button" data-filter=".cajas-suma">Cajas de suma</button>
							<button class="button" data-filter=".montajes">Montajes</button>
							<button class="button" data-filter=".patas">Patas</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 offset-md-1">
				<div class="row grid">
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->

					<!-- <div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/AX300/PA140021.JPG" class="img-fluid">
							</figure>
							<span class="t-produ">AX300 - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/AX300/AX300.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/BigBertha/DSC_0705.JPG" class="img-fluid">
							</figure>
							<span class="t-produ">BigBertha - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/BigBertha/BIGBERTHA.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/Centurion/Centurion-SD-SR-60.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Centurion - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Centurion/Centurion-AT-Print.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/Endurance/endurance.png" class="img-fluid">
							</figure>
							<span class="t-produ">Endurance - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Endurance/B-TEK_EnduranceAT_Web.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/InMotion/InMotion_AM.png" class="img-fluid">
							</figure>
							<span class="t-produ">InMotion - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/InMotion/BTEK_InMotion_Literature_Web.pdf" target="_blank">Descargar PDF</a>
						</div> 
					</div>-->
					
					<div >
						<div class="button-group filter-button-group list-produ">
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".ippsa">
									<img  src="assets/images/ippsa-gris.png" class="img-fluid">
								</button>
							</div>
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".avery">
									<img  src="assets/images/p-avery.png" class="img-fluid">
								</button>
							</div>
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".vpg">
									<img  src="assets/images/VPG.png" class="img-fluid">
								</button>
							</div>
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".pavone">
									<img  src="assets/images/pavone.jpg" class="img-fluid">
								</button>
							</div>
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".rinstrum">
									<img  src="assets/images/rinstrum.jpg" class="img-fluid">
								</button>
							</div>
							<div class="col-md-4 marcas item-producto">
								<button class="is-checked btn-logo" data-filter=".zemic">
									<img  src="assets/images/Zemic_Logo-azul.png" class="img-fluid">
								</button>
							</div>
						</div>
					</div>
					
					<!-- <div class="col-md-4 marcas item-producto">
						<div class="card-p">
							<figure>
								<img src="assets/images/ippsa-gris.png" class="img-fluid">
							</figure>
						</div>
					</div> -->



					<div class="col-md-4 producto item-producto avery camionera basculas ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/Kiosko/AVS-7.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Kiosko - Avery Weigh-Tronix</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Brochure_centurion_ippsa.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto avery camionera basculas ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/WeighTronix/WeighTronix.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Weightonix - Avery Weigh-Tronix</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Brochure_centurion_ippsa.pdf" target="_blank">Descargar PDF</a>
							<!-- <a class="btn btn-descarga" href="assets/images/basculas/camioneras/TruckScale_L_09940-0012.pdf" target="_blank">Descargar PDF</a> -->
						</div>
					</div>

					<div class="col-md-4 item-producto btek ferro basculas ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/Ferrocarrileras/RailTrack/Single-Digital-FL.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">RailTrack - B-Tek</span>
							<p>Básculas / Ferrocarrileras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/Ferrocarrileras/Brochure_bascula_ferrocarrilera_2.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery ferro basculas ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/Ferrocarrileras/RLP/rlp.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">RLP - Avery Weigh-Tronix</span>
							<p>Básculas / Ferrocarrileras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/Ferrocarrileras/Brochure_bascula_ferrocarrilera_2.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery ferro basculas ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/Ferrocarrileras/StreamLIne/Streamline.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">SteamLine - Avery Weigh-Tronix</span>
							<p>Básculas / Ferrocarrileras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/Ferrocarrileras/Brochure_bascula_ferrocarrilera_2.pdf" target="_blank">Descargar PDF</a>
							<!-- <a class="btn btn-descarga" href="assets/images/basculas/StreamLIne/streamline_L_500905.pdf" target="_blank">Descargar PDF</a> -->
						</div>
					</div>
					<div class="col-md-4 item-producto avery ferro basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/Ferrocarrileras/WeighLIne/Weighline.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">WeighLine - Avery Weigh-Tronix</span>
							<p>Básculas / Ferrocarrileras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/Ferrocarrileras/Brochure_bascula_ferrocarrilera_2.pdf" target="_blank">Descargar PDF</a>
							<!-- <a class="btn btn-descarga" href="assets/images/basculas/WeighLIne/Weighline_L_09553-0002.pdf" target="_blank">Descargar PDF</a> -->
						</div>
					</div>
					<div class="col-md-4 item-producto btek  piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/4Square/4-Square.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">4Square - B-Tek</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/4Square/FOURSQUARE-sm.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto btek  piso basculas ippsa  todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/Clydesdale/Clyde-CS-Ramp.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">CLydesdale - B-Tek</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/Clydesdale/CLYDESDALE-sm.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto btek piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/DrumScale/drum.JPG" class="img-fluid">
							</figure>
							<span class="t-produ">Drumscale - B-Tek</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/DrumScale/DRUM-sm.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/DS/Deck.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">DS - Avery Weigh-Tronix</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/DS/DeckScale_DS_spec_500452.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery piso basculas ippsa  todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/FCQH/fcqh.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">FCQH - Avery Weigh-Tronix</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/FCQH/FCQH_Spec_500628.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto btek piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/Hi-Impact/HiImpact3.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Hi Impact - B-Tek</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/Hi-Impact/HIIMPACT-COIL.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/Maxdec/maxdec.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Maxdec - Avery Weigh-Tronix</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/Maxdec/MaxDec_spec_500475.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/Prodec/pro-dec.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Prodec - Avery Weigh-Tronix</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/Prodec/ProDecDSL_spec_500476.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					
					<div class="col-md-4 item-producto avery piso basculas  ippsa todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/piso/ippsa/bascula-ippsa.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Báscula de piso - Ippsa</span>
							<p>Básculas / Piso</p>
							<a class="btn btn-descarga" href="assets/images/basculas/piso/ippsa/bascula_piso.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto avery mesa basculas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/mesa/3700/3700LP.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">3700 - Avery Weigh-Tronix</span>
							<p>Básculas / Mesa</p>
							<a class="btn btn-descarga" href="assets/images/basculas/mesa/3700/3700LP_spec_500527.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto mesa basculas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/mesa/Contadoras/675.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Contadoras - GSE</span>
							<p>Básculas / Mesa</p>
							<a class="btn btn-descarga" href="assets/images/basculas/mesa/Contadoras/GSECounting_L_500356.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery mesa basculas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/mesa/Diamond/Diamond.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Diamond - Avery Weigh-Tronix</span>
							<p>Básculas / Mesa</p>
							<a class="btn btn-descarga" href="assets/images/basculas/mesa/Diamond/BSbases_spec_500857.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery mesa basculas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/mesa/Torsion/torsion.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Torsion - Avery Weigh-Tronix</span>
							<p>Básculas / Mesa</p>
							<a class="btn btn-descarga" href="assets/images/basculas/mesa/Torsion/torsionBSFBSG_spec_500856.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery mesa basculas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/mesa/ZQ375/ZQ375.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ZQ375 - Avery Weigh-Tronix</span>
							<p>Básculas / Mesa</p>
							<a class="btn btn-descarga" href="assets/images/basculas/mesa/ZQ375/zq375_L_500766.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<!-- C E L D A S -->
					<!-- C E L D A S -->
					<!-- C E L D A S -->
					<!-- C E L D A S -->
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/92-93/92-93.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">92-93 - Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/92-93/92-93.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/220/220.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">220 - Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/220/220.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/ASC/ASC.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ASC- Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/ASC/ASC.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/ASC2/ASC2.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ASC2 - Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/ASC2/asc2.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/65088/65088.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65088- Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/65088/65088.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/65114/65114.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65114 - Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/65114/65114.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/RLC/rlc_s.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">RLC</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/RLC/RLC.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg compresion celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/compresion/LCD/LCD.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">LCD - Vpg Transducers</span>
							<p>Celdas / Compresión</p>
							<a class="btn btn-descarga" href="assets/images/celdas/compresion/LCD/LCD.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/380/380.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">380 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/380/380.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1002/1002.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1002 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1002/1002.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1004/1004.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1004 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1004/1004.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1006/1006.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1006 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1006/1006.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1010-1015/1010-1015.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1010-1015 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1010-1015/1010-1015.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1022/1022.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1022 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1022/1022.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1030/1030.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1030 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1030/1030.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1040-41/1040-1041.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1040-41 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1040-41/1040-41.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1042/1042.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1042 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1042/1042.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1042/1042.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1042 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1042/1042.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1130/1130.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1130 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1130/1130.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1140/1140.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1140 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1140/1140.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1142/1142.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1142 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1142/1142.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1242/1242.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1242 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1242/1242.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1250/1250.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1250 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1250/1250.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1252/1252.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1252 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1252/1252.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1260/1260.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1260 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1260/1260.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1263/1263.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1263 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1263/1263.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1265/1265.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1265 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1265/1265.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1320/1320.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1320 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1320/1320.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1330/1330.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1330 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1330/1330.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/1510/1510.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">1510 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/1510/1510.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/60051/60051.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60051 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/60051/60051.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/60060/60060.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60060 - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/60060/60060.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/HOC/HOC.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">HOC - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/HOC/HOC.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/HPS/HPS.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">HPS - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/HPS/HPS.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/LOC/LOC.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">LOC - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/LOC/LOC.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg single celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/single/LPS/LPS.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">LPS - Vpg Transducers</span>
							<p>Celdas / Single Point</p>
							<a class="btn btn-descarga" href="assets/images/celdas/single/LPS/LPS.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto vpg damped celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/damped/240/240.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">240 - Vpg Transducers</span>
							<p>Celdas / Damped </p>
							<a class="btn btn-descarga" href="assets/images/celdas/damped/240/240.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg damped celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/damped/9010/9010.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9010 - Vpg Transducers</span>
							<p>Celdas / Damped </p>
							<a class="btn btn-descarga" href="assets/images/celdas/damped/9010/9010.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/355/355.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">355 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/355/355.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/3510/3510.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">3510 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/3510/3510.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/5103/5103.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">5103 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/5103/5103.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/5123/5123.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">5123 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/5123/5123.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/5203/5203.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">5203 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/5203/5203.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/5223/5223.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">5223 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/5223/5223.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/5303/5303.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">5303 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/5303/5303.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/9103/9103.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9103 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/9103/9103.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/9123/9123.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9123 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/9123/9123.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/9203/9203.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9203 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/9203/9203.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/9223/9223.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9223 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/9223/9223.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/60040/60040.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60040 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/60040/60040.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/60058/60058.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60058 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/60058/60058.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/60060/60060.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60060 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/60060/60060.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65016/65016.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65016 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65016/65016.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65016/65016.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65016 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65016/65016.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65016-0104W/65016-W.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65016-0104W - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65016-0104W/65016-W.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65023/65023.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65023 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65023/65023.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65040-1127W/65040-1127W.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65040-1127W - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65040-1127W/65040-1127W.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65040W/65040W.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65040W - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65040W/65040W.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65058/65058.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65058 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65058/65058.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65058S/65058S.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65058S - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65058S/65058S.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65083/65083.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65083 - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65083/65083.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/65083H/65083H.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">65083H - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/65083H/65083H.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/SHB/SHB.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">SHB - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/SHB/SHB.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/SQB/SQB.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">SQB - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/SQB/SQB.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto vpg share celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/share/SQB-H/SQB-H.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">SQB-H - Vpg Transducers</span>
							<p>Celdas / Share Beams </p>
							<a class="btn btn-descarga" href="assets/images/celdas/share/SQB-H/SQB-H.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto tipo-s celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/tipo-s/363/363.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">363</span>
							<p>Celdas / Tipo S </p>
							<a class="btn btn-descarga" href="assets/images/celdas/tipo-s/363/363.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto tipo-s celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/tipo-s/9363/9363.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">9363</span>
							<p>Celdas / Tipo S </p>
							<a class="btn btn-descarga" href="assets/images/celdas/tipo-s/9363/9363.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto tipo-s celdas todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/celdas/tipo-s/60001/60001.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">60001</span>
							<p>Celdas / Tipo S </p>
							<a class="btn btn-descarga" href="assets/images/celdas/tipo-s/60001/60001.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					

					

					<!-- I N D I C A D O R E S -->
					<!-- I N D I C A D O R E S -->
					<!-- I N D I C A D O R E S -->
					<!-- I N D I C A D O R E S -->

					<!-- <div class="col-md-4 item-producto serie indicadores">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/560/GSE-560-562.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">GSE 560-562</span>
							<p>Indicadores / Serie Go</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/560/GSE560series_spec_5003781.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto serie indicadores">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/660/GSE-660.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">GSE 660</span>
							<p>Indicadores / Serie Go</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/660/GSE660series_spec_500379.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div> -->
					<div class="col-md-4 item-producto avery zm indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/ZM200/ZM201.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ZM200  - Avery Weigh-Tronix</span>
							<p>Indicadores / ZM</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/ZM200/zm201_L_501106.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery zm indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/ZM300/avery-weigh-tronix-zm300-series-indicator-672x480.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ZM300  - Avery Weigh-Tronix</span>
							<p>Indicadores / ZM</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/ZM300/zm300series_L_500828.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery zm indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/ZM300/avery-weigh-tronix-zm300-series-indicator-672x480.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">ZM300  - Avery Weigh-Tronix</span>
							<p>Indicadores / ZM</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/ZM300/zm300series_L_500828.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto avery zm indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/ZM400/ZM405.png" class="img-fluid">
							</figure>
							<span class="t-produ">ZM400  - Avery Weigh-Tronix</span>
							<p>Indicadores / ZM</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/ZM400/zm400_L_501390.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto rinstrum indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/R300/rinstrum-r320-digital-indicator.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">R300 - Rinstrum</span>
							<p>Indicadores / Rinstum</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/R300/R300-744-110.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto rinstrum indicadores todos pavone">
						<div class="card-p">
							<figure>
								<img src="assets/images/indicadores/R400/60.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">R400 - Rinstrum</span>
							<p>Indicadores / Rinstum</p>
							<a class="btn btn-descarga" href="assets/images/indicadores/R400/R400/R400-752-100.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<!-- A C C E S O R I O S -->
					<!-- A C C E S O R I O S -->
					<!-- A C C E S O R I O S -->
					<!-- A C C E S O R I O S -->

					<div class="col-md-4 item-producto cables accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/cable/FAWM206023.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">FAWM 206023 - Guardian Load Cells</span>
							<p>Accesorios / Cables</p>
						</div>
					</div>
					<div class="col-md-4 item-producto cables accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/cable/FCAB206045.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">FCAB 206045 - Guardian Load Cells</span>
							<p>Accesorios / Cables</p>
						</div>
					</div>

					<div class="col-md-4 item-producto btek montajes accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/montajes/DETM-SS-_-CS.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">DETM-SS y CS - B-Tek</span>
							<p>Accesorios / Montajes</p>
						</div>
					</div>
					<div class="col-md-4 item-producto btek montajes accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/montajes/EZTM-SS-_-CS.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">EZTM-SS y CS - B-Tek</span>
							<p>Accesorios / Montajes</p>
						</div>
					</div>
					<div class="col-md-4 item-producto btek montajes accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/montajes/SETM-SS-_-CS.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">SETM-SS y CS - B-Tek</span>
							<p>Accesorios / Montajes</p>
						</div>
					</div>
					<div class="col-md-4 item-producto patas accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/patas/IMG_1830.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Modelo 1</span>
							<p>Accesorios / Patas</p>
						</div>
					</div>
					<div class="col-md-4 item-producto patas accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/patas/IMG_1955.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Modelo 2</span>
							<p>Accesorios / Patas</p>
						</div>
					</div>
					<div class="col-md-4 item-producto patas accesorios todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/accesorios/patas/PataRotula.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Pata Rótula</span>
							<p>Accesorios / Patas</p>
						</div>
					</div>

					<!-- P A V O N E -->
					<!-- P A V O N E -->
					<!-- P A V O N E -->
					<!-- P A V O N E -->
					<div class="col-md-4 item-producto pavone todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/pavone/DAT100.png" class="img-fluid">
							</figure>
							<span class="t-produ">DAT 100/200</span>
							<!-- <p>Accesorios / Patas</p> -->
							<a class="btn btn-descarga" href="assets/images/pavone/DAT_100-200_Pavone_SIstem_2.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					
					<div class="col-md-4 item-producto pavone todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/pavone/DAT1400.png" class="img-fluid">
							</figure>
							<span class="t-produ">DAT 1400</span>
							<!-- <p>Accesorios / Patas</p> -->
							<a class="btn btn-descarga" href="assets/images/pavone/DAT_1400_Pavone_SIstem.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto pavone todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/pavone/uwt_6008.png" class="img-fluid">
							</figure>
							<span class="t-produ">UWT 6008</span>
							<!-- <p>Accesorios / Patas</p> -->
							<a class="btn btn-descarga" href="assets/images/pavone/Uwt_6008_Pavone_SIstem.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto pavone todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/pavone/DATS400.png" class="img-fluid">
							</figure>
							<span class="t-produ">DAT S 400</span>
							<!-- <p>Accesorios / Patas</p> -->
							<a class="btn btn-descarga" href="assets/images/pavone/DAT_s_400_Pavone_SIstem.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>
					<div class="col-md-4 item-producto pavone todos">
						<div class="card-p">
							<figure>
								<img src="assets/images/pavone/mc-315.png" class="img-fluid">
							</figure>
							<span class="t-produ">MC315</span>
							<!-- <p>Accesorios / Patas</p> -->
							<a class="btn btn-descarga" href="assets/images/pavone/MC315_Pavone_SIstem.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>



				</div> <!-- cierra row -->
			</div> <!-- cierra col -->
		</div> 
	</div>
</section>








<?php include('footer.php') ?>
<?php include('bottom.php') ?>
