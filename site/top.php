<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="HandheldFriendly" content="True">

	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="assets/images/phone.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/images/tablet.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/images/retina.png">

	<link rel="stylesheet" href="assets/css/styles.css">
	
	<?php include('meta.php'); ?>

</head>
<body>
