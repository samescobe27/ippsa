$(function(){

	// VALIDACION Y ENVIO
	$( "#forma-contacto" ).validate({
		errorElement: "small",
		errorClass: "text-danger",
		rules: {
			nombre: "required",
			apellidos: "required",
			email: {
				required: true,
				email: true
			},
			telefono: "required"
		},
		messages: {
			nombre: "Este campo es requerido",
			email: {
				required: "Ingresa un correo electrónico ",
				email: "Tu dirección de correo electrónico no es válida"
			},
			telefono: "Este campo es requerido"
		},
		success: "valid",
		submitHandler: function(currentForm) {

			var form = $(currentForm),
				data = new FormData(currentForm),
				button = form.find("[type=submit]");

			// ---------------------------------------------------------------
			var jqxhr = $.ajax({
				url: 'process.php',
				type: 'POST',
				data: data,
				contentType: false,
				processData: false,
				cache: false,
				beforeSend: function(){
					button.prop("disabled", true);
					button.text("Enviando");
				}
			});
			jqxhr.done(function( data ) {

				console.log(data);

				button.prop("disabled", false);
				button.text("Enviar");

				if( data.status_code == 200 ) {

					//MOSTRAR MENSAJE
					Swal.fire({
						icon: 'success',
						title: 'Thank you!',
						text: data.message
					});

					form[0].reset(); // reset form
					grecaptcha.reset(); // reset captcha
				} else {
					//MOSTRAR MENSAJE
					Swal.fire({
						icon: 'error',
						title: 'Oops!',
						text: data.message
					});
				}
			});
			jqxhr.fail( function() {
				button.prop("disabled", false);
				button.text("Enviar");
			});
			// ---------------------------------------------------------------
		}

	});

});

