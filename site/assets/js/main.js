
$(document).ready(function() {
  $("#btn-buscar-header").on("click", function(e){
    e.preventDefault();
    // clearForms();
    $("#form-buscar-header").submit();
  });

    // animaciones
	new WOW().init();

    	// scroll
	$(".scroll").click(function(event){
        // event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
        return false;
    });

    // quick search regex
var qsRegex;



    // init Isotope
    // var $grid = $('.grid').isotope({
    //     itemSelector: '.item-producto',
    //     layoutMode: 'fitRows',
    //     filter: '.marcas'
    // });


    



   // bind filter button click
$('.filters-button-group').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $grid.isotope({ filter: filterValue });
  });
  // change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });
  
      

      
   // filter items on button click
    $('.filter-button-group').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });
    });

 
    // layout Isotope after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.isotope('layout');
    });


    // mobile nav toogle
     $(".hamburger").on('click', function(e){
         e.preventDefault();
         $(this).toggleClass('is-active');
         $('.mobile-nav-cont').slideToggle(900, 'easeOutExpo');
     });
 
     var navCopy = $('.main-nav').html();
     $('.mobile-nav-cont .minner .navi').html(navCopy);

     var headerInfoCopy = $('.main-header .top').html();
	$('.mobile-nav-cont .minner .info').html(headerInfoCopy);
    
     // Bodegas interior
    
  
    	// video popups
	$('.popup-video').magnificPopup({
		disableOn: 0,
		type: 'iframe'
	});

    var $carousel = $('.carousel').flickity({
        pageDots: false,
        prevNextButtons: false,
        freeScroll: true,
        wrapAround: true
        });
 
    
 
 });