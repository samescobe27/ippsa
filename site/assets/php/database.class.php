<?php
//http://culttt.com/2012/10/01/roll-your-own-pdo-php-class/
/*function __autoload($class_name){
    require_once $class_name . '.php';
}*/

//conection
class Config{

    // Localhost
    private $host   = 'localhost';
    private $user   = 'root';
    private $pass   = 'root';
    private $dbname = 'ippsa_admin';

    // Prod
    // private $host   = 'localhost';
    // private $user   = 'wi580426_admin';
    // private $pass   = 'Ipssa*2016';
    // private $dbname = 'wi580426_laravel';

    // SHOTMTY
    /*
        private $host   = 'localhost';
        private $user   = 'shotmty_admin';
        private $pass   = 'sxq^A_cpqya7';
        private $dbname = 'shotmty_javer_admin';
    */

    protected $dbh;

    public function __construct(){
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        // Set options
        $options = array(
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_PERSISTENT       => true,
            PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8'
        );
        // Create a new PDO instanace
        try{
            return $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        // Catch any errors
        catch(PDOException $e){
            echo json_encode(array("success"=>false, "message"=>$e->getMessage()));
            exit;
        }
    }
}

// database
class Database extends Config{

    private $stmt;

    public function __construct()
    {
        parent::__construct();
    }

    // Query
    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
    }
    // Bind
    public function bind($param, $value, $type = null){
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    // Execute
    public function execute(){
        return $this->stmt->execute();
    }
    // Result
    public function resultset(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
    // Row count
    public function rowCount(){
        return $this->stmt->rowCount();
    }
    // Last insert id
    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

    /* Transactions*/

    // Begin transaction
    public function beginTransaction(){
        return $this->dbh->beginTransaction();
    }
    // End transaction
    public function endTransaction(){
        return $this->dbh->commit();
    }
    // Cancel transaction
    public function cancelTransaction(){
        return $this->dbh->rollBack();
    }

    /*Debug Dump Parameters*/
    public function debugDumpParams(){
        return $this->stmt->debugDumpParams();
    }
}

?>
