<?php

header("Content-type: application/json;charset=utf8");

date_default_timezone_set('America/Mexico_City');
//phpmailer
require 'assets/php/phpmailer/PHPMailerAutoload.php';

//Conexion 1
require 'assets/php/database.class.php';

// Secret Key
$secret_key = '6LeX7MYUAAAAAHZO1-lEWsRt-sHLFsbZmPhG_Lh0';

// VERIFICAR CAPTCHA
if( isset($_POST['g-recaptcha-response']) && strlen($_POST['g-recaptcha-response']) > 0 )
{
	$gRecaptchaResponse = $_POST['g-recaptcha-response'];
} else {
	echo json_encode(array('status_code'=>500, 'message' =>'Haz click en la casilla de seguridad para continuar'));
	exit;
}

$data = array(
    'secret' => $secret_key,
    'response' => $gRecaptchaResponse,
    'remoteip' => $_SERVER['REMOTE_ADDR'],
);

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$response = json_decode($response);

if( $response->success == false )
{
	echo json_encode(array('status_code'=>500, 'message' =>'Response is false'));
	exit;
}

// phpmailer---------------------------------------------------------------------
$nombre = htmlspecialchars( $_POST['nombre'] );
$apellidos = htmlspecialchars( $_POST['apellidos'] );
$email = htmlspecialchars( $_POST['email'] );
$telefono = htmlspecialchars( $_POST['telefono'] );
$como_te_enteraste = htmlspecialchars( $_POST['como_te_enteraste'] );
$cuando_comenzar = htmlspecialchars( $_POST['cuando_comenzar'] );
$mensaje = htmlspecialchars( $_POST['mensaje'] );
$created_at = date("Y-m-d H:i:s");

$mail = new PHPMailer;

$mail->Subject = 'Contacto | Foil Multicolor';

// $mail->addAddress('humberto.shot@gmail.com', 'Información');
$mail->addAddress('foilmulticolormty@gmail.com', 'Contacto');

$mail->From = 'humberto.shot@gmail.com';
$mail->FromName = 'Info';

// prepare email body text
$Body = "";

$Body .= "<strong>Nombre:</strong> ";
$Body .= $nombre;
$Body .= "<br><br>";

$Body .= "<strong>Apellidos:</strong> ";
$Body .= $apellidos;
$Body .= "<br><br>";

$Body .= "<strong>Email:</strong> ";
$Body .= $email;
$Body .= "<br><br>";

$Body .= "<strong>Teléfono:</strong> ";
$Body .= $telefono;
$Body .= "<br><br>";

$Body .= "<strong>¿Cómo te enteraste de nosotros?:</strong> ";
$Body .= $como_te_enteraste;
$Body .= "<br><br>";

$Body .= "<strong>¿Cuándo te interesa comenzar?</strong> ";
$Body .= $cuando_comenzar;
$Body .= "<br><br>";

$Body .= "<strong>Mensaje:</strong> ";
$Body .= $mensaje;


$mail->isHTML(true); // Set email format to HTML

$mail->CharSet = 'UTF-8';
$mail->Body    = $Body;


if(!$mail->send()) {
	//echo json_encode(array('status_code'=>500, 'message' =>$mail->ErrorInfo));
	echo json_encode(array('status_code'=>500, 'message' =>'Ocurrio un error intenta de nuevo.'));
} else {
	echo json_encode(array('status_code'=>200, 'message' =>'Gracias!!! Tu mensaje se ha enviado correctamente.'));
}

//phpmailer---------------------------------------------------------------------

// saveData($nombre, $apellidos, $email, $telefono, $como_te_enteraste, $cuando_comenzar, $mensaje, $created_at);

exit;


// Funciones
function saveData($nombre, $apellidos, $email, $telefono, $como_te_enteraste, $cuando_comenzar, $mensaje, $created_at)
{
    $database = new Database();
    $database->query('
    	INSERT INTO contacto (
			nombre,
			apellidos,
			email,
			telefono,
			como_te_enteraste,
			cuando_comenzar,
			mensaje,
			created_at)
    	VALUES (
			:nombre,
			:apellidos,
			:email,
			:telefono,
			:como_te_enteraste,
			:cuando_comenzar,
			:mensaje,
			:created_at)
    ');

    $database->bind(':nombre', $nombre);
	$database->bind(':apellidos', $apellidos);
	$database->bind(':email', $email);
	$database->bind(':telefono', $telefono);
	$database->bind(':como_te_enteraste', $como_te_enteraste);
	$database->bind(':cuando_comenzar', $cuando_comenzar);
	$database->bind(':mensaje', $mensaje);
	$database->bind(':created_at', $created_at);

    if (!$database->execute()) {

       mail('juancarlos.shot@gmail.com', 'Central Abastos - Contacto', 'La info de contacto no se esta guardando en la base de datos.');

    }
}

?>
