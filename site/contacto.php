<?php

// meta tags
$titulo = "Contacto";
$descrip = "Desarrollemos juntos ese proyecto";
$imagen = "";


include('top.php');
include('header.php');

?>

<section class="banner-inicio banner-contacto banner-productos" style="background-image: url(assets/images/banner-contacto.jpg);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-12 col-md-8 col-lg-4  block wow fadeInRight">
				<h1>Desarrollemos <br>juntos ese proyecto</h1>
				<span>Contáctanos</span>
			</div>
		</div>
	</div>
</section> 

<section class="contacto-intro" >
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col-lg-4 offset-lg-1 back">
                <h2 class="titulo wow fadeInLeft">Oficina <br>Monterrey</h2>
                <p><strong>Dirección</strong></p>
                <p>Arnulfo Portales #2235<br>
                   Col. Ferrocarrilaera<br>
                   Monterrey<br>
                   Nuevo León, México</p>

                <p><strong>Teléfono</strong></p>
                <a class="" href="tel:83521892">
                    Tel: (81) 83521892
                </a>
                <a class="" href="tel:9269197">
                    Otro: 9269197 Cve. 5279
                </a>
            </div>
            <div class="col-lg-7 nopadding">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1797.1264210315842!2d-100.34000354323403!3d25.72915089882628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662954f4e1f7e53%3A0x31edcc57d84c3715!2sIPPSA%20-%20Instrumentacion%20y%20Proyectos%20de%20Pesaje%20S.A.%20de%20C.V!5e0!3m2!1ses-419!2smx!4v1577390131212!5m2!1ses-419!2smx" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            <div class="col-lg-5 offset-lg-1 forma-block">
                <?php include('inc-form.php') ?>
              
			</div>
		</div>
	</div>
</section>




<?php include('footer.php') ?>
<?php include('bottom.php') ?>