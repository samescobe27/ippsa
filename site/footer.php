<footer class="main-footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12  col-md-12 col-lg-3 block">
                <a href=".">
                    <img class="" src="assets/images/ippsa-azul.png" alt="IPPSA" width="160" height="37">
                </a>
            </div>
            <div class="col-12 col-md-4 col-lg-3 block contact">
                <span class="title-footer">Contacto</span>
                <p>Email: </p>
                <a href="contacto@ippsa.com.mx">contacto@ippsa.com.mx</a>

                <p>Teléfono: </p>
                <a href="8183521892"> (81)8352 1892</a>
            </div>
            <div class="col-12 col-md-4 col-lg-3 block">
                <span class="title-footer">Accesos rápidos</span>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                    </li>
                    <li>
                        <a href="productos.php">Productos</a>
                    </li>
                    <li>
                        <a href="contacto.php">Contacto</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3 block">
                <span class="title-footer">Dirección</span>
                <p>Arnulfo Portales #2235 Col. Ferrocarrilaera Monterrey Nuevo León, Mexico</p>
            </div>
        </div>
    </div>
</footer>


<div><div id="whatsAppWidget" style="position: fixed; right: 20px; bottom: 3px; float: left; margin-bottom: 60px; z-index:20;"><a href="https://api.whatsapp.com/send?phone=528711373929" target="_blank"><div style="float: right; height: 50px; width: 50px; border-radius: 50%; overflow: hidden; box-shadow: 2px 2px 6px rgba(0,0,0,0.4); text-align: center; line-height: 50px; color: white; background-color: #4dc247;"><svg style="display: inline; fill: white" viewBox="0 0 32 32" width="41px" height="50px" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></svg></div><span style="border-radius: 3px; box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.3); display: block; float: left; margin-top: 6px; padding: 8px; max-width: 160px; min-width: 34px; font-size: 12px; font-weight: 500; color: #8a8a8a; background-color: #ffffff; box-sizing: content-box; line-height: initial; clear: both; word-break: break-word;">¡Haz tu pedido ya!<br>Horario: de L-V 9:00 a 7:00 Sábados 9:00 a 1:00</span></a></div></div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="assets/js-min/scripts.js"></script>