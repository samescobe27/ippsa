<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<title>IPPSA</title>
<meta name="description" content="<?php echo $descrip; ?>">

<meta property="og:title" content="">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $actual_link; ?>">
<meta property="og:site_name" content="IPPSA">
<meta property="og:description" content="<?php echo $descrip; ?>">
<?php if ( $imagen !== "" ): ?>
	<meta property="og:image" content="<?php echo $imagen; ?>" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="1400" />
	<meta property="og:image:height" content="850" />
<?php endif; ?>
<meta property="og:locale" content="es_MX" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@" />
<meta name="twitter:title" content="<?php echo $titulo; ?>" />
<meta name="twitter:description" content="<?php echo $descrip; ?>" />
<?php if ( $imagen !== "" ): ?>
	<meta name="twitter:image" content="<?php echo $imagen; ?>" />
<?php endif; ?>
