<?php

// meta tags
$titulo = "Inicio";
$descrip = "Diseño y fabricación de básculas especiales";
$imagen = "";


include('top.php');
include('header.php');

?>





	<section class="banner" id="inicio">
		<div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-slides=">.slide" data-cycle-timeout="5000" data-cycle-pager=".cycle-pager">
		 
		
			<div class="slide" style="background-image: url('assets/images/banner-1.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1>Conviértete <br>en distribuidor.</h1>
								<span>Estamos buscando distribuidores <br>para toda la República Mexicana.</span>
								<a class="btn btn-naranja mt-3" href="contacto-distribuidor.php">Contáctanos para más información</a>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <div class="slide" style="background-image: url('assets/images/banner-02.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1>Distribuidores oficiales</h1>
								<span>de Celdas de carga de VPG Transducers</span>
								<a class="btn btn-naranja mt-3" href="productos.php">Conoce nuestras celdas</a>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div class="slide" style="background-image: url('assets/images/banner-3.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1>Distribuidores oficiales <img src="assets/images/Logo_VPG.png" class="img-fluid"></h1>
								
								<a class="btn btn-naranja mt-3" href="productos.php">Conoce nuestras celdas</a>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div class="slide" style="background-image: url('assets/images/banner-04.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="verde col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1 class="verde">Distribuidores <br> oficiales 
								</h1>
								<span class="gris-text">de transmisores de peso <br> Pavone Sistemi</span> 
								<a class="btn btn-naranja mt-3" href="productos.php">Conoce nuestros productos</a>
								<figure>
								<img class="logo" widht="100%" height="80px" src="assets/images/logo-pavone-v.png">
								</figure>
							</div>
		    			</div>
		    		</div>
		    	</div>
			</div> 
			<div class="slide" style="background-image: url('assets/images/banner-05.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1 >Distribuidores <br> oficiales 
								</h1>
								<span>de celdas de carga <br> Zemic</span> 
								<a class="btn btn-naranja mt-3" href="productos.php">Conoce nuestros productos</a>
								<figure>
								<img class="logo2" widht="100%" height="71px" src="assets/images/Zemic_Logo.png">
								</figure>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div> 
			<div class="slide" style="background-image: url('assets/images/banner-6.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
							<div class="col-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4 block wow fadeInRight">
								<h1 >Distribuidores <br> oficiales 
								</h1>
								<span>de productos <br> Utilcell</span> 
								<a class="btn btn-naranja mt-3" href="productos.php">Conoce nuestros productos</a>
								<figure>
								<img class="logo2" widht="100%" height="33px" src="assets/images/logoutilcell.png" style="margin-top:120px;">
								</figure>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div class="cycle-pager"></div>
		</div>
		
	</section>


<section class="cards-inicio">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10 col-xl-10">
				<div class="row">
					<div class="col-md-4 col-lg-4">
						<div class="card wow zoomIn">
							<figure>
								<img  src="assets/images/basculas.png" alt="Básculas Industriales">
								<figcaption>Básculas <br>Industriales</figcaption>
							</figure>
							<p>Básculas para camiones regulares y fuera de carretera, pesaje de ejes de forma estática o dinámica, básculas de alto impacto, pesaje en banda, ferrocarrileras, para altas temperaturas, pesaje en tolva.</p>
						</div>
					</div>
					<div class="col-md-4 col-lg-4">
						<div class="card wow zoomIn">
							<figure>
								<img class="line" src="assets/images/stock.png" alt="Stock de equipo y refacciones">
								<figcaption>Stock de equipo<br> y refacciones</figcaption>
							</figure>
							<p>Mas de 1000 celdas de carga en stock listas para embarcarse, stock de los principales modelos de indicadores, amplio stock de accesorios como montajes, cajas de suma, cable, etc.</p>
						</div>
					</div>
					<div class=" col-md-4 col-lg-4">
						<div class="card wow zoomIn">
							<figure>
								<img src="assets/images/software.jpg" alt="Software a la medida">
								<figcaption>Software<br> a la medida</figcaption>
							</figure>
							<p>Sistemas de etiquetado, control de inventario, preparación de lote, transporte y almacenamiento, programa camionero de entrada y salida, sistemas personalizados de básculas para camiones.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-12">
				<video width="100%" height="400px" autoplay loop playsinline>
					<source src="assets/images/ippsa.mp4" type="video/mp4">
				</video> 
			</div>
		</div>
	</div>
</section>

<section class="garantia" id="garantia">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12">
				<h2 class="sub blanco">Garantía IPPSA</h2>
			</div>
			<div class="col-lg-10 ">
				<div class="row">
					
					<div class="col-lg-3 block wow fadeIn" data-wow-delay="0.5s">
						<figure>
							<img src="assets/images/i-envios.png" alt="Todo México " widht="47" height="44">
						</figure>
						<span>Todo México</span>
						<p>Tenemos cobertura en toda la república mexicana tanto en envíos de productos como en servicios.</p>
						<p>Tenemos clientes en todo el territorio nacional.</p>
					</div>
					<div class="col-lg-3 block wow fadeIn" data-wow-delay="0.5s">
						<figure>
							<img src="assets/images/i-stock.png" alt="Stock" widht="47" height="44">
						</figure>
						<span>Stock</span>
						<p>Manejamos las mejores marcas del mercado en básculas, indicadores y celdas de carga, lo cual nos permite hacer envíos el mismo día de ser requerido.</p>
					</div>
					<div class="col-lg-3 block wow fadeIn" data-wow-delay="0.5s">
						<figure>
							<img src="assets/images/i-soluciones.png" alt="Solución Total"  widht="47" height="44">
						</figure>
						<span>Solución Total</span>
						<p>Desarrollamos soluciones integrales que van desde el pesaje simple hasta aplicaciones personalizadas para batcheo, formulación, etiquetado, pesaje dinámico, camionero y otros.</p>
					</div>
					<div class="col-lg-3 block wow fadeIn" data-wow-delay="0.5s">
						<figure>
							<img src="assets/images/i-soporte.png" alt="Soporte" widht="36" height="45">
						</figure>
						<span>Soporte</span>
						<p>Soporte personalizado y de atención directa con nuestros clientes, todos nuestros clientes son importantes.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="nosotros" id="nosotros">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="sub mb-4">Nosotros</h3>
			</div>
			<div class="col-md-6  wow fadeInUp" data-wow-delay="0.5s">
			
				<p>En IPPSA ponemos más de 50 años de experiencia conjunta sobre la mesa, nos especializamos en el diseño y fabricación de básculas especiales para la industria pesada.</p>
				<p>Proporcionamos una inspección completa, prueba y calibración de todas las marcas y modelos de equipos de pesaje. Balanzas de precisión y básculas industriales. También ofrecemos programas de mantenimiento y auditorías ISO.</p>
				<p>Somos distribuidores de importantes marcas como:</p>
			</div>
			<div class="col-md-6">
				<ul>
					<li><img src="assets/images/awt.jpg"></li>
					<li><img src="assets/images/Zemic_Logo-azul.png"></li>
					<li><img src="assets/images/rinstrum.jpg"></li>
					<li><img src="assets/images/sensotronics2.png"></li>

					<li class="icon-3"><img src="assets/images/reveree.png"></li>
					<li class="icon-3"><img src="assets/images/tedea.png"></li>
					<li class="icon-3"><img src="assets/images/celtron.png"></li>

					<li><img src="assets/images/pavone.jpg"></li>
					<li><img src="assets/images/btek.jpg"></li>
					
					
					
				</ul>
			</div>
		</div>
	</div>
</section>


<section class="servicios" id="servicios" style="background-image: url(assets/images/banner-servicios.jpg);">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-6 col-lg-6  wow fadeIn" data-wow-delay="0.5s">
				<h4 class="sub">Servicios</h4>
				<p class="text-1">Somos especialistas en todo tipo de básculas y soluciones de pesaje e instrumentación. Estos son algunos de los servicios que ofrecemos:</p>
				<a class="btn btn-naranja mt-3" href="contacto.php">Cotiza tu proyecto</a>
			</div>
			<div class="col-12 col-md-6 col-lg-6  wow fadeIn" data-wow-delay="0.5s">
				<span>Pólizas de Mantenimiento</span>
				<p>Ofrecemos pólizas de mantenimiento preventivo periodico, con esto se garantiza la correcta operación de sus equipos.</p>

				<span>Calibración</span>
				<p>Ofrecemos servicios de calibración en bajo, mediano y alto alcance así como de verificación PROFECO.</p>

				<span>Instalación</span>
				<p>Ofrecemos todos nuestros productos bajo el concepto de llave en mano</p>


				<span>Consultoría de soluciones</span>
				<p>Ofrecemos servicios de consultoría para que en conjunto seleccionamos el mejor equipo para sus necesidades.</p>
			</div>
		</div>
	</div>
</section> 

<script src="http://malsup.github.com/jquery.cycle2.js"></script>
<?php include('footer.php') ?>
<?php include('bottom.php') ?>