<?php

require './assets/php/database.class.php';

$buscar = ($_GET['buscar']) ? $_GET['buscar'] : null;
$brand_id = ($_GET['brand_id']) ? $_GET['brand_id'] : null;
$category_id = ($_GET['category_id']) ? $_GET['category_id'] : 1;
$subcategory_id = ($_GET['subcategory_id']) ? $_GET['subcategory_id'] : null;

// Productos
$query_productos = new Database;

$sql = '
SELECT products.*,
product_categories.name as nombre_categoria,
product_subcategories.name as nombre_subcategoria
FROM products
LEFT JOIN product_categories
ON products.category_id = product_categories.id
LEFT JOIN product_subcategories
ON products.subcategory_id = product_subcategories.id
WHERE products.status_id = :status_id
';

if($buscar):
	$sql .= ' AND products.name LIKE :buscar';
endif;

if($brand_id):
	$sql .= ' AND products.brand_id = :brand_id';
endif;

if($category_id):
	$sql .= ' AND products.category_id = :category_id';
endif;

if($subcategory_id):
	$sql .= ' AND products.subcategory_id = :subcategory_id';
endif;


$sql .= ' ORDER BY products.name ASC';

$query_productos->query($sql);
$query_productos->bind(':status_id', 1);

if($buscar):
	$query_productos->bind(':buscar', '%' . $buscar . '%');
endif;

if($brand_id):
	$query_productos->bind(':brand_id', $brand_id);
endif;

if($category_id):
	$query_productos->bind(':category_id', $category_id);
endif;

if($subcategory_id):
	$query_productos->bind(':subcategory_id', $subcategory_id);
endif;


$productos = $query_productos->resultset();

// Marcas
$query_marcas = new Database();
$query_marcas->query('SELECT * FROM product_brands ORDER BY name ASC');

$marcas = $query_marcas->resultset();


// Categorias
$query_categorias = new Database();
$query_categorias->query('SELECT * FROM product_categories ORDER BY created_at DESC');

$categorias = $query_categorias->resultset();

// Subategorias
$query_subcategorias = new Database();
$query_subcategorias->query('SELECT * FROM product_subcategories ORDER BY name ASC');

$subcategorias = $query_subcategorias->resultset();


// meta tags
$titulo = "Productos";
$descrip = "";
$imagen = "";


include('top.php');
include('header.php');

?>

<section class="banner-productos" style="background-image: url(assets/images/banner-productos.jpg);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-12 col-md-8 col-lg-6  block wow fadeInRight">
				<h1>Conoce nuestros <br>productos</h1>
				<span>Celdas de carga, Básculas, Indicadores, Accesorios y Diseños especiales</span>
			</div>
		</div>
	</div>
</section> 



<section class="productos">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<h2>Productos</h2>
			</div>
			<div class="col-md-10 search-products">
				<form id="form-buscar" action="productos.php">
                    <input class="quicksearch button-search " name="buscar" id="buscar" type="text" placeholder="Buscar producto" class="form-control" value="<?= ($buscar) ? $buscar: '' ?>">
					
					<!-- Marca -->
					<select style="display:none" name="brand_id" id="f-marca">
						<option value="" selected></option>
						<?php
							foreach($marcas as $marca):
						?>
							<option <?= ($brand_id && $brand_id == $marca['id']) ? 'selected':'' ?> value="<?php echo $marca['id'] ?>"><?php echo $marca['name'] ?></option>
						<?php endforeach; ?>
					</select>

					<!-- categoria -->
					<select style="display:none" name="category_id" id="f-categoria">
						<option value="" selected></option>
						<?php
							foreach($categorias as $categoria):
						?>
							<option <?= ($category_id && $category_id == $categoria['id']) ? 'selected':'' ?> value="<?php echo $categoria['id'] ?>"><?php echo $categoria['name'] ?></option>
						<?php endforeach; ?>
					</select>
					
					<!-- Subcategoria -->
					<select style="display:none" name="subcategory_id" id="f-subcategoria">
						<option value="" selected></option>
						<?php
							foreach($subcategorias as $subcategoria):
						?>
							<option <?= ($subcategory_id && $subcategory_id == $subcategoria['id']) ? 'selected':'' ?> value="<?php echo $subcategoria['id'] ?>"><?php echo $subcategoria['name'] ?></option>
						<?php endforeach; ?>
					</select>

                </form>
				<a href="#" id="btn-buscar" class="search-icon  d-md-block d-lg-block">
					<img src="assets/images/search-2.png"  widht="28" height="20" >
				</a>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-2">
				<div class="menu-productos">
			
					<a class="btn btn-titulo" data-toggle="collapse" href="#marcas" role="button" aria-expanded="false" aria-controls="marcas">
						Marcas <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
					</a>
					
					<div class="collapse show button-group filter-button-group" id="marcas">
						<div class="row button-group filter-button-group list-produ">
							<!-- <button class="button is-checked" data-filter=".marcas">Marcas</button> -->
                            <?php foreach($marcas as $marca): ?>
                                <button class="button marca" data-filter="<?php echo $marca['id'] ?>"><?php echo $marca['name'] ?></button>
                            <?php endforeach; ?>
						</div>
					</div>
					
					<!-- Categorias y Subcategorias -->
                    <?php
                        foreach($categorias as $categoria):

                    ?>
                        <a class="btn btn-titulo" data-toggle="collapse" href="#categoria-<?php echo $categoria['id'] ?>" role="button" aria-expanded="false" aria-controls="<?php echo $categoria['name'] ?>">
                            <?php echo $categoria['name'] ?> <img class="drop" src="assets/images/i-drop.png" widht="14" height="8">
                        </a>
                        
                        <div class="collapse show button-group filter-button-group" id="categoria-<?php echo $categoria['id'] ?>">
                            <div class="row button-group filter-button-group list-produ">
                                <!-- <button class="button categoria" data-filter="<?php echo $categoria['id'] ?>">Todos</button> -->
                                <?php

                                    $qSub = new Database;
                                    $qSub->query('SELECT * FROM product_subcategories WHERE product_category_id = :cid ORDER BY name ASC');
                                    $qSub->bind(':cid', $categoria['id']);

                                    $subcategorias = $qSub->resultset();

                                    foreach($subcategorias as $subcategoria):

                                ?>
                                    <button class="button sub" data-filter="<?php echo $subcategoria['id'] ?>"><?php echo $subcategoria['name'] ?></button>
                                <?php
                                    endforeach; //subcategorias
                                ?>
                            </div>
                        </div>

                    <?php
                        endforeach;//categorias
                    ?>
				</div>
			</div>
			<div class="col-md-9 offset-md-1">
				<div class="row grid">
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->
					<!-- B A S C U L A S -->

					<!-- <div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/AX300/PA140021.JPG" class="img-fluid">
							</figure>
							<span class="t-produ">AX300 - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/AX300/AX300.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/BigBertha/DSC_0705.JPG" class="img-fluid">
							</figure>
							<span class="t-produ">BigBertha - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/BigBertha/BIGBERTHA.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/Centurion/Centurion-SD-SR-60.jpg" class="img-fluid">
							</figure>
							<span class="t-produ">Centurion - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Centurion/Centurion-AT-Print.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/Endurance/endurance.png" class="img-fluid">
							</figure>
							<span class="t-produ">Endurance - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/Endurance/B-TEK_EnduranceAT_Web.pdf" target="_blank">Descargar PDF</a>
						</div>
					</div>

					<div class="col-md-4 item-producto btek camionera basculas">
						<div class="card-p">
							<figure>
								<img src="assets/images/basculas/camioneras/InMotion/InMotion_AM.png" class="img-fluid">
							</figure>
							<span class="t-produ">InMotion - B-Tek</span>
							<p>Básculas / Camioneras</p>
							<a class="btn btn-descarga" href="assets/images/basculas/camioneras/InMotion/BTEK_InMotion_Literature_Web.pdf" target="_blank">Descargar PDF</a>
						</div> 
					</div>-->
					
					<?php /* if(!$buscar && !$brand_id && !$category_id && !$subcategory_id): ?>
					<div >
						<div class="row button-group filter-button-group list-produ">
                            <?php foreach($marcas as $marca): ?>
                                <div class="col-md-4 marcas item-producto">
                                    <button class="is-checked btn-logo marca" data-filter="<?php echo $marca['id'] ?>">
                                        <img src="http://ippsa.com.mx/laravel-admin/uploads/productos/marcas/<?php echo $marca['image'] ?>" class="img-fluid">
                                    </button>
                                </div>
                            <?php endforeach; ?>
						</div>
					</div>
					<?php endif; */ ?>
					
					<!-- <div class="col-md-4 marcas item-producto">
						<div class="card-p">
							<figure>
								<img src="assets/images/ippsa-gris.png" class="img-fluid">
							</figure>
						</div>
					</div> -->

					<?php if(count($productos) > 0): ?>
						<?php foreach($productos as $producto): ?>
							<div class="col-md-4 producto item-producto <?= ($buscar) ? 'buscar' : '' ?> <?= ($producto['brand_id']) ? 'marca-' . $producto['brand_id'] : '' ?> <?= ($producto['category_id']) ? 'categoria-' . $producto['category_id'] : '' ?> <?= ($producto['subcategory_id']) ? 'subcategoria-' . $producto['subcategory_id'] : '' ?> todos">
								<div class="card-p">
									<figure>
										<img src="http://ippsa.com.mx/laravel-admin/uploads/productos/<?php echo $producto['image'] ?>" class="img-fluid">
									</figure>
									<span class="t-produ"><?php echo $producto['name'] ?></span>
									<p><?php echo $producto['nombre_categoria'] ?> / <?php echo $producto['nombre_subcategoria'] ?></p>
									<?php if($producto['pdf']): ?>
										<a class="btn btn-descarga" href="http://ippsa.com.mx/laravel-admin/uploads/productos/pdf/<?php echo $producto['pdf'] ?>" target="_blank">Descargar PDF</a>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					<?php else: ?>
						<p>No hay productos ...</p>
					<?php endif; ?>

				</div> <!-- cierra row -->
			</div> <!-- cierra col -->
		</div>
	</div>
</section>






<?php include('footer.php') ?>

<script>
    $(function(){

		var form = $("#form-buscar"); // id del form

        $("#btn-buscar").on("click", function(e){
			e.preventDefault();
			$('#form-buscar select').val("");
            form.submit();
		});
		
		// click en una marca
		$(".marca").on("click", function(e){
			e.preventDefault();

			clearForms();

			var marca = $(this).data('filter');

			$("#f-marca option[value='"+marca+"']").attr("selected", true);
            form.submit();
		});

		// click en una categoria
		$(".categoria").on("click", function(e){
			e.preventDefault();
			
			clearForms();

			var categoria = $(this).data('filter');

			$("#f-categoria option[value='"+categoria+"']").attr("selected", true);
            form.submit();
		});
		
		// click en una subcategoria
		$(".sub").on("click", function(e){
			e.preventDefault();

			clearForms();

			var sub = $(this).data('filter');

			$("#f-subcategoria option[value='"+sub+"']").attr("selected", true);
            form.submit();
        });
		
	});


	function clearForms()
	{
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$('#form-buscar select').val("");
	}
</script>

<?php include('bottom.php') ?>
