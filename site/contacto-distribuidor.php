<?php

// meta tags
$titulo = "Contacto";
$descrip = "Desarrollemos juntos ese proyecto";
$imagen = "";


include('top.php');
include('header.php');

?>






<section class="banner-productos" style="background-image: url(assets/images/banner-1.jpg);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-12 col-md-8 col-lg-6  block wow fadeInRight">
            <h1>¡Conviértete <br>en un distribuidor!</h1>
                <span>Estamos buscando distribuidores de IPPSA <br>para toda la República Mexicana.</span>
                <a class="btn btn-naranja mt-3" href="contacto.php">Contáctanos para más información</a>
			</div>
		</div>
	</div>
</section> 


<section class="contacto-intro" >
	<div class="container nopadding">
		<div class="row">
			<div class="col-lg-5 offset-lg-1 back">
                <h2 class="titulo wow fadeInLeft">¿Quieres ser <br>distribuidor?</h2>
                <p class="mt-5">Estamos buscando distribuiders de IPPSA. Contáctanos si estás interesado y nos comunicaremos contigo.</p>
            </div>
            <div class="col-lg-6 forma-block">
                 <?php include('inc-form.php') ?>
            </div>
            
		</div>
	</div>
</section>




<?php include('footer.php') ?>
<?php include('bottom.php') ?>