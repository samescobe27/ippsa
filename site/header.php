<header class="main-header">
	<div class="container block">
		<div class="row align-items-center">
			<div class="col-6 col-sm-4 col-md-3 col-lg-3 main–logo">
				<div class="logo">
					<a href=".">
						<img class="" src="assets/images/ippsa-gris.png" alt="IPPSA" width="160" height="37">
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-8 col-md-9 col-lg-9 info">
				<div class="top">
					<form id="form-buscar-header" action="productos.php">
						<input class="button-search d-none d-md-block d-lg-block" name="buscar" type="text" placeholder="Buscar producto" class="form-control">
					</form>
					<a id="btn-buscar-header" href="#" class="search-icon d-none d-md-block d-lg-block">
						<img src="assets/images/lupa.svg"  widht="28" height="20" >
					</a>
					<a class="block-btn d-none d-md-block d-lg-block" href="" target="_blank" rel="noopener noreferrer">
						<img class="mr-1" src="assets/images/i-mail.png" widht="21" height="14" alt="Mail">
					</a>
					
					<div class="mobile-toggler-cont d-lg-none">
						<div class="hamburger-wrap">
							<button class="hamburger hamburger--slider hamburger--accessible js-hamburger" type="button">
								<span class="hamburger-box">
									<span class="hamburger-inner"></span>
								</span>
								<span class="hamburger-label">Menu</span>
							</button>
						</div>
					</div>
				</div>
				<div class="main-nav">
					<nav>
						<ul>
							<li>
								<a href=".">Inicio</a>
							</li>
							<li>
								<a class="scroll" href="index.php#nosotros">Nosotros</a>
							</li>
							<li>
								<a class="scroll" href="index.php#servicios">Servicios</a>
							</li>
							<li>
								<a href="productos.php">Productos</a>
							</li>
							<li>
								<a href="contacto.php" >Contacto</a>
							</li>
						</ul>
					</nav>
				</div>
			</div> 
			<div class="col-12">
				<div class="mobile-nav-cont">
					<div class="minner">
						<div class="navi"></div>
						<div class="info"></div>
					</div>
				</div>	
			</div>
		</div>
	</div>

</header>

