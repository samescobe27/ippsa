<form class="form-inline" id="forma-contacto">
    <span class="sub wow fadeIn">Contáctanos</span>
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input name="nombre" id="nombre" type="text" class="form-control">
            </div>
        </div>
        
        <div class="col-12 col-sm-12">
            <div class="form-group">
                <label for="email">Correo</label>
                <input name="email" id="email" type="email" class="form-control">
            </div>
        </div>
        <div class="col-12 col-sm-12">
            <div class="form-group">
                <label for="asunto">Asunto</label>
                <input name="asunto" id="asunto" type="text" class="form-control">
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label for="mensaje">Mensaje</label>
                <textarea name="mensaje" id="mensaje" class="form-control" rows="5"></textarea>
            </div>
        </div>
        <!-- <div class="col-12 ">
            <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6LeX7MYUAAAAAOo7ygToLM0gVqShZHZrpIsLYXue"></div>
            </div>
        </div> -->
        <div class="col-12">
            <button type="submit" class="btn btn-celeste mt-4">ENVIAR</button>
        </div>
    </div>
</form>