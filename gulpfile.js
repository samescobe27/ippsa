/***
iniciar browsersync en la terminal https://www.browsersync.io/docs/command-line/#proxy-example

browser-sync start --proxy 'localhost:8888/lanterra/site' --serveStatic 'public' --files 'site/' --open
***/

const gulp = require('gulp'),
      sass = require('gulp-sass'),
      cleancss = require('gulp-clean-css'),
      concat = require('gulp-concat'),
	  uglify = require('gulp-uglify'),
	  notify = require("gulp-notify");


// sass
function estilos() {
	return gulp.src('./site/assets/scss/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleancss())
        .pipe(gulp.dest('./site/assets/css'))
        .pipe(notify('estilos 🎨'))
}
gulp.task(estilos);


// codigo
function js(){
	return gulp.src(
      	[
			'./node_modules/jquery/dist/jquery.min.js',
			'./node_modules/popper.js/dist/umd/popper.js',
			'./node_modules/bootstrap/dist/js/bootstrap.js',	
			'./node_modules/flickity/dist/flickity.pkgd.min.js',
			'./node_modules/isotope-layout/dist/isotope.pkgd.min.js',
			'./node_modules/imagesloaded/imagesloaded.pkgd.js',
			'./node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
			'./site/assets/js/easing.js',
			'./site/assets/js/wow.js',
			'./node_modules/jquery-validation/dist/jquery.validate.js',
			'./node_modules/jquery-validation/dist/additional-methods.js',
			'./node_modules/sweetalert2/dist/sweetalert2.js',
			'./site/assets/js/validar.js',
			'./site/assets/js/jquery.cycle2.min.js',
            './site/assets/js/jquery.cycle2.swipe.js',
			'./site/assets/js/main.js',
      	]
	)
    .pipe(concat('scripts.js'))
    .pipe(uglify())
	.pipe(gulp.dest('./site/assets/js-min'))
	.pipe(notify('js 💾'))
}
gulp.task(js);


gulp.task('default', function () {
	gulp.watch('./site/assets/scss/*', gulp.series('estilos'));
	gulp.watch('./site/assets/js/main.js', gulp.series('js'));
});
